var Game = new Object();
var Player1 = new Object();
var Player2 = new Object();
Player1.score = 0;
Player2.score = 0;
var Board = new Object();
Game.local = false;
Board.size = 13;
// variable for Gomoku
var jrecord = new JGO.Record(Board.size);
var jboard = jrecord.jboard;
var jsetup = new JGO.Setup(jboard, JGO.BOARD.large);
var player = JGO.BLACK; // next player
var ko = false, lastMove = false; // ko coordinate and last move coordinate
var lastHover = false, lastX = -1, lastY = -1; // hover helper vars