$( document ).ready(function() {

	var transition = false;

	webSocket.connect();



	$("body").css("background", "#1a1a1a");

	function getPreviousConfiguration(current, previous) {
		if ($(current).hasClass('visible'))
		{
			transition = true;
			$(current).transition({
				 onComplete : function() {
				 	$(previous).transition();
				 	transition = false;
				 }
			});
		}
	}

	// Mouse scroll detection
	$(window).bind('mousewheel', function(event) {
	    if (event.originalEvent.wheelDelta >= 0) {
	        if (!transition)
	        {
	        	getPreviousConfiguration("#versus", "#username");
	        	getPreviousConfiguration("#multiplayer", "#versus");
	        	getPreviousConfiguration("#machine", "#versus");
	        	getPreviousConfiguration("#game", "#versus");
	        	getPreviousConfiguration("#remote", "#multiplayer");
	        	getPreviousConfiguration("#secondUsername", "#multiplayer");
	        	getPreviousConfiguration("#join", "#remote");
	        	getPreviousConfiguration("#create", "#remote");
	        }
	    }
	});

	if (isConnected(webSocket))
	{
		//show setup
		$('#setup')
		  	.modal({
			    blurring: true
			})
			.modal({		})
			.modal('setting', 'closable', false)
			.modal('show')
		;
	}

	// Valid the username
	$("#username .button").click(function(){
		$("#username").transition({
			 onComplete : function() {
			 	Player1.id = Math.random().toString(36).substr(2, 9);
			 	Player1.name = $("#username input").val();
			 	Player1.type = "player";
			 	$("#versus").transition();
			 }
		});
	});
	// Click on versus human
	$("#versus .human").click(function(){
		$("#versus").transition({
			 onComplete : function() {
			 	Game.type = "Multiplayer";
			 	$("#multiplayer").transition();
			 }
		});
	});
	// Click on versus machine
	$("#versus .machine").click(function(){
		$("#versus").transition({
			onComplete : function() {
				$("#machine").transition();
			}
		})
	});
	// Click on AI vs AI
	$("#machine .machine").click(function(){
		$("#machine").transition({
			 onComplete : function() {
			 	// Player1
			 	Game.join = false;
			 	Game.id = Math.random().toString(36).substr(2, 9);
			 	Player1.name = "AI 1";
			 	Player1.type = "AI";
			 	if (isConnected(webSocket))
			 	{
				 	var send = {
				 		connection: {
					 		Game,
					 		Player: Player1,
					 		Board
					 	}
				 	};
				 	webSocket.send(send);
			 	}
			 	// PLayer2
			 	Game.join = true;
			 	Player2.id = Math.random().toString(36).substr(2, 9);
			 	Player2.name = "AI 2";
			 	Player2.type = "AI";
			 	if (isConnected(webSocket))
			 	{
				 	var send = {
				 		connection: {
					 		Game,
					 		Player: Player2,
					 		Board
					 	}
				 	};
				 	webSocket.send(send);
				}
				Game.local = true;
			 	$("#game").transition();
			 	$('.ui.modal').modal('hide');
			 }
		});
	});
	// Click on human vs AI
	$("#machine .human").click(function(){
		$("#machine").transition({
			 onComplete : function() {
			 	// Player1
			 	Game.join = false;
			 	Game.id = Math.random().toString(36).substr(2, 9);
			 	if (isConnected(webSocket))
			 	{
				 	var send = {
				 		connection: {
					 		Game,
					 		Player: Player1,
					 		Board
					 	}
				 	};
				 	webSocket.send(send);
			 	}
			 	// PLayer2
			 	Game.join = true;
			 	Player2.id = Math.random().toString(36).substr(2, 9);
			 	Player2.name = "AI";
			 	Player2.type = "AI";
			 	if (isConnected(webSocket))
			 	{
				 	var send = {
				 		connection: {
					 		Game,
					 		Player: Player2,
					 		Board
					 	}
				 	};
				 	webSocket.send(send);
				}
				Game.local = false;
			 	$("#game").transition();
			 	$('.ui.modal').modal('hide');
			 }
		});
	});
	// Click on multiplayer remote
	$("#multiplayer .remote").click(function(){
		$("#multiplayer").transition({
			 onComplete : function() {
			 	$("#remote").transition();
			 }
		});
	});
	// Click on multiplayer local
	$("#multiplayer .local").click(function(){
		$("#multiplayer").transition({
			 onComplete : function() {
			 	$("#secondUsername").transition();
			 }
		});
	});
	// Valid Username Second Player
	$("#secondUsername .button").click(function(){
		$("#secondUsername").transition({
			 onComplete : function() {
			 	// Player1
			 	Game.join = false;
			 	Game.id = Math.random().toString(36).substr(2, 9);
			 	if (isConnected(webSocket))
			 	{
				 	var send = {
				 		connection: {
					 		Game,
					 		Player: Player1,
					 		Board
					 	}
				 	};
				 	webSocket.send(send);
			 	}
			 	// PLayer2
			 	Game.join = true;
			 	Player2.id = Math.random().toString(36).substr(2, 9);
			 	Player2.name = $("#secondUsername input").val();
			 	Player2.type = "player";
			 	if (isConnected(webSocket))
			 	{
				 	var send = {
				 		connection: {
					 		Game,
					 		Player: Player2,
					 		Board
					 	}
				 	};
				 	webSocket.send(send);
				}
				Game.local = true;
				$("#game").transition();
			 	$('.ui.modal').modal('hide');
			 }
		});
	});
	// Click on join party
	$("#remote .join").click(function(){
		$("#remote").transition({
			 onComplete : function() {
			 	Game.join = true;
			 	$("#join").transition();
			 }
		});
	});
	// Click on join after join party
	$("#join .input button").click(function(){
		$("#join").transition({
			 onComplete : function() {
			 	$("#game").transition();
			 	$('.ui.modal').modal('hide');
			 	if (isConnected(webSocket))
			 	{
				 	Game.id = $("#join .input input").val();
				 	var send = {
				 		connection: {
					 		Game,
					 		Player: Player1,
					 		Board
					 	}
				 	};
				 	webSocket.send(send);
				 	waitPlayer(true, Player1.id);
				}
			 }
		});
	});
	// Click on create party
	$("#remote .create").click(function(){
		$("#remote").transition({
			 onComplete : function() {
			 	Game.id = Math.random().toString(36).substr(2, 9);
			 	Game.join = false;
			 	$("#create .input input").val(Game.id);
			 	$("#create").transition();
			 }
		});
	});
	// Click on play after create party
	$("#create .input button").click(function(){
		$("#create").transition({
			 onComplete : function() {
			 	$("#game").transition();
			 	$('.ui.modal').modal('hide');
			 	if (isConnected(webSocket))
			 	{
				 	var send = {
				 		connection: {
					 		Game,
					 		Player: Player1,
					 		Board
					 	}
				 	};
				 	webSocket.send(send);
				 	waitPlayer(true, Player1.id);
			 	}
			 }
		});
	});

	// Click on Next Game
	$("#win .button").click(function(){
		console.log("Wait Game");
		tryNextGame();
	});
	// Click on error
	$("#warning .close").click(function(){
		$("#warning").transition();
	});

});