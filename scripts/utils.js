function tryToReconnect()
{
	if (webSocket.isConnected() == false)
		location.reload();
	else
	{
		  	$('#badConnection').modal('hide');
		  	//show setup
			$('#setup')
			  	.modal({
				    blurring: true
				})
				.modal({		})
				.modal('setting', 'closable', false)
				.modal('show')
			;
	}
}

// Check if the server is connected to the webservice
function isConnected(webSocket)
{
	if (webSocket.isConnected() == false)
	{
		$('.ui.modal').modal('hide');

		  	$('#badConnection')
		      	.modal({
				    blurring: true
				})
				.modal('setting', 'closable', false)
				.modal('show')
			;

		return (false);
	}
	return (true);
}

function waitPlayer(val, id)
{
	var player = getOtherPlayerById(id);
	if (val && Game.local == false && player.name != "" && typeof player.name != 'undefined')
	{
		$('#wait h2').html("Waiting for "+player.name);
		$('#wait').removeClass("active transition hidden");
		$('#wait')
	      	.modal({
			    blurring: true
			})
			.modal('setting', 'closable', false)
			.modal('show')
		;
	}
	else if (val && Game.local == false)
	{
		$('#wait h2').html("Waiting for the other player");
		$('#wait')
	      	.modal({
			    blurring: true
			})
			.modal('setting', 'closable', false)
			.modal('show')
		;
	}
	else
		$('#wait').modal('hide');
}
