

	function displayWarning(header, message)
	{
		$("#warning .header").html(header);
		$("#warning p").html(message);
		$("#warning").transition('show');
	}

	function displayError(error, val)
	{
		$("#error .message").html(error);
		if (val)
		{
			$('#error')
		      	.modal({
				    blurring: true
				})
				.modal('setting', 'closable', false)
				.modal('show')
			;
		}
		else
			$('#error').modal('hide');
	}

	function tryNextGame()
	{
		waitPlayer(true, Player1.id);
		var send = {
		    reload: {
		      Game: {
		        id: Game.id
		      },
		      Player: Player1
		    }
		  };
		webSocket.send(send);
		console.log(send);
	}

	function nextGame()
	{
		var send = {
		    reload: {
		      Game: {
		        id: Game.id
		      }
		    }
		  };
		webSocket.send(send);
		console.log(send);
	}

	function displayWin(message, val)
	{
		$("#win .message").html(message);
		if (val)
		{
			$('#win')
		      	.modal({
				    blurring: true
				})
				.modal('setting', 'closable', false)
				.modal('show')
			;
		}
		else
			$('#win').modal('hide');
	}

	function getOtherPlayerById(id)
	{
		if (id == Player1.id)
			return (Player2);
		else
			return (Player1);
	}

	function getPlayerColor(color)
	{
		if (color == "white")
			return(JGO.WHITE);
		else
			return(JGO.BLACK);
	}

	function getPlayerByColor(color)
	{
		if (getPlayerColor(Player1.color) == color)
			return(Player1);
		else
			return(Player2);
	}

	function putStone(coordX, coordY, player)
	{
	    jboard.setType(new JGO.Coordinate(coordX, coordY), player);
	    lastHover = false;
	}

	function displayInfo()
	{			
		if (Player1.name != "none")
			$(".player1 .username").html(Player1.name);
		if (Player2.name != "none")
			$(".player2 .username").html(Player2.name);
		$(".player1 .color").html(Player1.color);
		$(".player2 .color").html(Player2.color);
		$(".player1 .score").html(Player1.score);
		$(".player2 .score").html(Player2.score);
	}

	//parse result
	function parseResult(receive)
	{
		console.log("parseResult:");
		if (receive.hasOwnProperty('connection'))
		{
			var connection = receive.connection;
			if (connection.Result.success)
			{
				if (Player1.id == connection.Player.id)
				{
					Player1.color = connection.Player.color;
					Player2.name = connection.Opponent.name;
					Player2.color = connection.Opponent.color;
				}
				else if (Player2.id == connection.Player.id)
				{
					Player2.color = connection.Player.color;
					Player1.name = connection.Opponent.name;
					Player1.color = connection.Opponent.color;
				}
				displayInfo();
			}
			else
			{
				console.log("Error: not connected !");
				console.log(connection.Result.message);
				displayError(connection.Result.message, true);
			}
		}
		else if (receive.hasOwnProperty('move'))
		{
			var move = receive.move;
			if (move.Player.win != "none")
			{
				if (move.Player.win == "draw")
					displayWin("Draw", true);
				else
				{
					if (Player1.color == move.Player.win)
					{
						Player1.score += 1;
						displayWin(Player1.name+" win !", true);
					}
					else
					{
						Player2.score += 1;
						displayWin(Player2.name+" loose !", true);
					}
				}
			}
			if (move.Result.success == true)
			{
				if (move.Player.win == "none")
				{
					console.log("Call Wait Player");
					waitPlayer(true, move.Player.id);
				}
				if (Player1.id == move.Player.id)
					player = getPlayerColor(Player1.color);
				else
					player = getPlayerColor(Player2.color);
				putStone(move.Coord.x, move.Coord.y, player);
			}
			else
			{
				console.log("Error: Can't move !");
				console.log(move.Result.message);
				displayWarning("Error in the move", move.Result.message);
			}
		}
		else if (receive.hasOwnProperty('yourTurn'))
		{
			var yourTurn = receive.yourTurn;
			if (yourTurn.Player.win != "none")
			{
				waitPlayer(false, yourTurn.Player.id);
				if (yourTurn.Player.win == "draw")
					displayWin("Equality", true);
				else
				{
					if (Player1.color == yourTurn.Player.win)
					{
						Player1.score += 1;
						displayWin(Player1.name+" win !", true);
					}
					else
					{
						Player2.score += 1;
						displayWin(Player2.name+" loose !", true);
					}
				}
			}
			if (Player1.id == yourTurn.Player.id)
			{
				player = getPlayerColor(Player1.color);
				Player2.name = yourTurn.Opponent.name;
				Player2.color = yourTurn.Opponent.color;
			}
			else
			{
				player = getPlayerColor(Player2.color);
				Player1.name = yourTurn.Opponent.name;
				Player1.color = yourTurn.Opponent.color;
			}
			displayInfo();
			if (yourTurn.Coord.x >= 0 && yourTurn.Coord.y >= 0)
			{
				player = (player == JGO.BLACK) ? JGO.WHITE : JGO.BLACK;
				jboard.setType(new JGO.Coordinate(yourTurn.Coord.x , yourTurn.Coord.y), player);
				player = (player == JGO.BLACK) ? JGO.WHITE : JGO.BLACK;
			}
			console.log("WaitPlayer False");
			waitPlayer(false, yourTurn.Player.id);
		}
		else if (receive.hasOwnProperty('reload'))
		{
			var reload = receive.reload;
			if (reload.Result.wait)
			{
				if (Game.local == false)
				{
					$('#next .message').html(Player2.name + " asks if you want to restart the game?")
					$('#next')
				      	.modal({
						    blurring: true
						})
						.modal('setting', 'closable', false)
						.modal('show')
					;
				}
				else
					nextGame();
			}
			else if (reload.Result.success)
			{
				resetMap();
				setPlayerColor(reload.Player.id, reload.Player.color);
				displayInfo();
				displayWin("", false);
				waitPlayer(true, reload.Player.id);
			}
			delete (reload);
		}

	}

	function setPlayerColor(id, color)
	{
		if (Player1.id == id)
			Player1.color = color;
		else if (Player2.id == id)
			Player2.color = color;
	}

	function resetMap()
	{
		for (var x = 0; x < Board.size; x++)
			for (var y = 0; y < Board.size; y++)
				jboard.setType(new JGO.Coordinate(x, y), JGO.CLEAR);
	}

	function tryToMove(x, y)
	{
	  var send = {
	    move: {
	      Game,
	      Player: getPlayerByColor(player),
	      Coord: {
	        x: x,
	        y: y
	      }
	    }
	  };
	  //console.log(send);
	  webSocket.send(send);
	}

	jsetup.setOptions({stars: {points:5}});
	jsetup.create('board', function(canvas) {
	  canvas.addListener('click', function(coord, ev) {

	    //console.log(coord);
	    tryToMove(coord.i, coord.j);

	  });

	  canvas.addListener('mousemove', function(coord, ev) {
	    if(coord.i == -1 || coord.j == -1 || (coord.i == lastX && coord.j == lastY))
	      return;

	    if(lastHover) // clear previous hover if there was one
	      jboard.setType(new JGO.Coordinate(lastX, lastY), JGO.CLEAR);

	    lastX = coord.i;
	    lastY = coord.j;

	    if(jboard.getType(coord) == JGO.CLEAR && jboard.getMark(coord) == JGO.MARK.NONE) {
	      jboard.setType(coord, player == JGO.WHITE ? JGO.DIM_WHITE : JGO.DIM_BLACK);
	      lastHover = true;
	    } else
	      lastHover = false;
	  });

	  canvas.addListener('mouseout', function(ev) {
	    if(lastHover)
	      jboard.setType(new JGO.Coordinate(lastX, lastY), JGO.CLEAR);

	    lastHover = false;
	  });
	});


	//listen websocket
	webSocket.listen(function(message) {
		console.log("Message:")
		console.log(message);
		parseResult(message);
    });